-module(gitweb).
-export([out/1]).

-include("$YAWS_DIR/include/yaws_api.hrl").

out(Arg) ->
   Pathinfo = Arg#arg.appmoddata,
   Env = [],
   yaws_cgi:call_cgi(Arg, undefined, script(), Pathinfo, Env).

script() ->
   "/usr/lib/cgi-bin/gitweb.cgi".

